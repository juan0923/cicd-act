terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "tstate"
    storage_account_name = "tstate27541"
    container_name       = "tstate"
    key                  = "terraform.state"
  }
}

provider "azurerm" {
  features {}
}


resource "azurerm_virtual_network" "network" {
  name                = "vnet"
  address_space       = ["10.0.0.0/16"]
  location            = "central us"
  resource_group_name = "tstate"
}
